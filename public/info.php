<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<link rel="stylesheet" href="searchbar.css" type="text/css">
	<style>
		table, th, td {
			border: 1px solid black;
		}
		a {
			text-decoration: none;
			display: inline-block;
			padding: 8px 16px;
		}

		a:hover {
			background-color: #ddd;
			color: black;
		}
		
		.normal {
			background-color: #4CAF50;
			color: white;
		}
		
		.previous {
			background-color: #4CAF50;
			color: white;
		}

		.next {
			background-color: #4CAF50;
			color: white;
		}

		.round {
			border-radius: 50%;
		}
		
		#pokemon td, #customers th {
			border: 1px solid #ddd;
			padding: 8px;
		}

		#pokemon tr:nth-child(even){background-color: #f2f2f2;}

		#pokemon tr:hover {background-color: #ddd;}

		#pokemon th {
			padding-top: 12px;
			padding-bottom: 12px;
			text-align: left;
			background-color: #4CAF50;
			color: white;
		}
	</style>
	<title>Pokemon information</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" type="text/css" href="style.css"/>
</head>
<body>
<?php
	$query = $_GET['query'] ?? ''; 	
    if($query == 0){
			$query = 802;
	}
	if($query == 803){
			$query = 1;
	}
	$base  = "http://pokeapi.co/api/v2/pokemon/";	
	$data = file_get_contents($base.$query.'/');			
	$pokemon = json_decode($data);
	
	$typeArr = '';
	$abilArr = '';
	$statArr = '';
	$moveArr = '';
				
	foreach($pokemon->types as $values)
	{
		$typeArr = $typeArr.' '.ucfirst($values->type->name).'<br>';
	}
	
	foreach($pokemon->abilities as $values)
	{
		$abilArr = $abilArr.' '.ucfirst($values->ability->name).'<br>';
	}
	
	foreach($pokemon->stats as $values)
	{
		$statArr = $statArr.' '.ucfirst($values->stat->name).':'. $values->base_stat.'<br>';
	}
	
	foreach($pokemon->moves as $values)
	{
		$moveArr = $moveArr.' '.ucfirst($values->move->name).', ';
	}

	echo'
	<br />
	<center>
	<h2> #'. $pokemon->id .' '. ucfirst($pokemon->name) .'</h2>	
	<br />
	<img src="'. $pokemon->sprites->front_default .'" width="150px" />					
	<img src="'. $pokemon->sprites->back_default .'" width="150px" />
	<img src="'. $pokemon->sprites->front_shiny .'" width="150px" />					
	<img src="'. $pokemon->sprites->back_shiny .'" width="150px" />
	<br />
	Original and Shiny pictures
	<br />
	
	<table id="pokemon" width="800px">
		<thead>
			<th width="200px">Height</th>
			<th width="200px">Weight</th>
			<th width="400px">Species</th>			
		</thead>		
		<tr>
			<td>
				'. $pokemon->height * 0.1 .' m
			</td>
			<td>
				'. $pokemon->weight * 0.1 .' kg
			</td>
			<td>
				'. $pokemon->species->name .'
			</td>
		</tr>
	</table>
	
	<br />
	
	<table id="pokemon" width="800px">
		<thead>		
			<th width="400px">Types</th>
			<th width="400px">Abilities</th>
		</thead>
		<tr>
			<td>
				'. $typeArr .'
			</td>
			<td>
				'. $abilArr .'
			</td>
		</tr>
	</table>
	
	<br />
	<table id="pokemon" width="800px">
		<thead>		
			<th width="200px">Stats</th>			
			<th width="600px">Moves</th>
		</thead>
		<tr>
			<td>
				'. $statArr .'
			</td>
			<td>
				'. $moveArr .'
			</td>
		</tr>
	</table>
	
	<br />
	<a href="info.php?query='. ($pokemon->id - 1) .'" class="previous">&laquo; Previous pokemon</a>
	<a href="index.php?query='. $pokemon->id.'" class="normal">Back to search</a>
	<a href="index.php" class="normal">Back to list</a>
	<a href="info.php?query='. ($pokemon->id + 1) .'" class="next">Next pokemon &raquo;</a>
	</center>
	<br />
	<br />
	'
?>	
</body>
</html>
