<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>	
	<link rel="stylesheet" href="searchbar.css" type="text/css">
	<script>
	$(document).ready(function(){
		$('#pokedex').dynatable();
	});
	</script>
	<style>
		table, th, td {
			border: 1px solid black;
		}
		a {
			text-decoration: none;
			display: inline-block;
			padding: 8px 16px;
		}

		a:hover {
			background-color: #ddd;
			color: black;
		}
		
		.normal {
			background-color: #4CAF50;
			color: white;
		}

		.previous {
			background-color: #4CAF50;
			color: white;
		}

		.next {
			background-color: #4CAF50;
			color: white;
		}

		.round {
			border-radius: 50%;
		}
		
		#pokemon td, #customers th {
			border: 1px solid #ddd;
			padding: 8px;
		}

		#pokemon tr:nth-child(even){background-color: #f2f2f2;}

		#pokemon tr:hover {background-color: #ddd;}

		#pokemon th {
			padding-top: 12px;
			padding-bottom: 12px;
			text-align: left;
			background-color: #4CAF50;
			color: white;
		}
	</style>
    <title>Pokedex</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" type="text/css" href="style.css"/>
</head>
<body>
	<center>
	<h2>Pokedex created by Mario Sandoval</h2>
	<h3>Enter the name or ID of the pokemon in the box.</h3>
	<br>
    <form action="index.php" method="GET">
		
			<input type="text" name="query" size="50" />
			<input type="submit" value="Search pokemon" />
		
    </form>
	<?php
		function curl_get_contents($url)
		{
			$ch = curl_init($url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
			$data = curl_exec($ch);
			curl_close($ch);
			return $data;
		}
	
		$query = $_GET['query'] ?? ''; 						
		$idmov = $_GET['idmov'] ?? 0; 
		$base  = "http://pokeapi.co/api/v2/pokemon/";		
	?>	
	<br>
	<table id="pokemon">
		<thead>
			<th>ID</th>
			<th>Image</th>			
			<th width="100px">Name</th>			
			<th width="200px">Type</th>			
		</thead>
	<tbody>
	<?php
		if($query == "")
		{	
			$iniVal = $idmov + 1;
			$endVal = $idmov + 10;
			
			for($id = $iniVal; $id <= $endVal; $id++){				
				$data = file_get_contents($base.$id.'/');			
				$pokemon = json_decode($data);
				$typeArr = '';
				
				foreach($pokemon->types as $values)
				{
					$typeArr = $typeArr.' '.ucfirst($values->type->name).'<br>';
				}
			
				echo '<tr>
				<td>
					'. $pokemon->id .'
				</td>
				<td>
					<img src="'. $pokemon->sprites->front_default .'" width="50px" />					
				</td>
				<td>
					<a href="info.php?query='.$pokemon->id.'">'. ucfirst($pokemon->name) .'</a>
				</td>			
				<td>
					'. $typeArr .'
				</td>				
				</tr>';
			}
		}
		
		else
		{			
			$data = @file_get_contents($base.strtolower($query).'/');			
			if($data == FALSE){
				echo 'No data available, please try to enter the full name of the pokemon or its ID number.';
			}
			
			else{
				
				$pokemon = json_decode($data);
				
				$idpkm = $pokemon->id;												
				
				$data = file_get_contents($base.$idpkm.'/');			
				$pokemon = json_decode($data);
				$typeArr = '';
				
				foreach($pokemon->types as $values)
				{
					$typeArr = $typeArr.' '.ucfirst($values->type->name).'<br>';
				}
			
				echo '<tr>
				<td>
					'. $pokemon->id .'
				</td>
				<td>
					<img src="'. $pokemon->sprites->front_default .'" width="50px" />					
				</td>
				<td>
					<a href="info.php?query='.$pokemon->id.'">'. ucfirst($pokemon->name) .'</a>
				</td>			
				<td>
					'. $typeArr .'
				</td>				
				</tr>';				
			}						
		}
	?>
	</tbody>
	</table>
	<br />
	<?php
		if($query == "")
		{
			if($idmov >= 10){
				echo'
					<a href="index.php?idmov='. ($idmov - 10) .'" class="previous">&laquo; Previous</a> &nbsp; &nbsp;';
			}
		}
		echo'<a href="index.php" class="normal">Start again!</a> &nbsp; &nbsp;';
		
		if($query == "")
		{
			echo'
				<a href="index.php?idmov='. ($idmov + 10) .'" class="next">Next &raquo;</a>
			';
		}
	?>
</body>
</html>